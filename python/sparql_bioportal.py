# -*- coding: utf-8 -*-
"""
Created on Mon Jul 23 15:06:57 2018

Querying BioPortal SPARQL EndPoint

@author: mmar
"""


from SPARQLWrapper import SPARQLWrapper, JSON


if __name__ == "__main__":
    sparql_service = "http://sparql.bioontology.org/sparql/"

    #To get your API key register at http://bioportal.bioontology.org/accounts/new
    api_key = "ca0787e8-0bd8-4538-846f-91ccccb748b4"

    #Some sample query.
    #En prefix go: le indicamos la ontología a la que queremos ir
    #En where go: seleccionamos la clase dentro de esa ontología
    query_string = """ 
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX snomed-term: <http://purl.bioontology.org/ontology/SNOMEDCT/>
    PREFIX go: <http://purl.bioontology.org/ontology/STY/>
    PREFIX skos: <http://bioportal.bioontology.org/metadata/def/>
    SELECT  ?subClassOf ?prefLabel?label
    FROM <http://bioportal.bioontology.org/ontologies/STY>
    WHERE
    {
       OPTIONAL{go:T071 rdfs:subClassOf ?subClassOf .}
       OPTIONAL{go:T071 rdfs:label ?label .}
       OPTIONAL{go:T071 skos:prefLabel  ?prefLabel.}
    }
        """
        
    query_1 = "SELECT * WHERE { ?s ?p ?o. }"
        

    sparql = SPARQLWrapper(sparql_service)
    sparql.addCustomParameter("apikey",api_key)
    sparql.setQuery(query_string)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    
    print(results["results"]["bindings"])
    
#    for result in results["results"]["bindings"]:
#        print('label: %s, definition: %s' % (result["label"]["value"], result["def"]["value"]))
        
        
        
   #Some sample query.
#    query_string2 = """ 
#    PREFIX omv: <http://omv.ontoware.org/2005/05/ontology#>
#    SELECT ?ont ?name ?acr
#    WHERE { ?ont a omv:Ontology;
#                 omv:acronym ?acr;
#                 omv:name ?name .
#    } 
#    """
#    sparql = SPARQLWrapper(sparql_service)
#    sparql.addCustomParameter("apikey",api_key)
#    sparql.setQuery(query_string2)
#    sparql.setReturnFormat(JSON)
#    results = sparql.query().convert()
#    for result in results["results"]["bindings"]:
#        print (result["ont"]["value"], result["name"]["value"], result["acr"]["value"])
        
        
        
    
        
        
        
        
        
        
        
        
        
        
        
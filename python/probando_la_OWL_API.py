# -*- coding: utf-8 -*-
"""
Created on Tue Jul 24 12:10:25 2018


@author: mmar and Sandro
"""

####################################################
# Para escribir label, isDefinedBy y definition para una clase de la ontología
from owlready2 import *
import sys
sys.setrecursionlimit(3000)
name_ontology = "grao.owl"

def add_LabelAndDef():

    
    onto = get_ontology('/home/khaosdev/AnacondaProjects/TFM/owl_project/python/Sandro_Code/GRO2_original.owl').load()
    print(len(list(onto.classes())))
    print(list(onto.classes())
    
    for c in onto.classes():
        cc = c.iri #Nos devuelve la uri completa
        print(cc)
        print(c.label)
        

        if (cc == "http://purl.obolibrary.org/obo/SO_0000704"):
            c.label = "Esta es de de mi cosecha"
            c.isDefinedBy.append("Basic Formal Ontology")
            
    with onto:
        class adddefinition(AnnotationProperty):
            pass
    adddefinition.comment = 'hola'
          
    for c in onto.classes():
        cc = str(c.iri)
        if (cc == "http://purl.obolibrary.org/obo/BFO_0000015"):
            c.definition.append("Ponemos aqui la definicion")
#    
#    
#    
    onto.save(file = "grao4.owl", format = "rdfxml")


    
add_LabelAndDef()

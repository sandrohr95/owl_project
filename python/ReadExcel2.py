#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  4 14:11:23 2018

@author: Sandro
"""

import openpyxl
from urllib.parse import unquote
from parse import *
import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile

#Leemos el archivo excel para obtener el valor de cada hiperlink asociado al nombre de las clases de la ontología
    
wb = openpyxl.load_workbook('class_equivalent.xlsx')
#print(wb.sheetnames) #Cogemos el norbre de la hoja excel
ws = wb['Existing GRO']

def readOldUrl():

    url = ws['A3'].value #Cogemos el valor (hiperlink) de la clase en la posición A3 en el excel
    decode = unquote(url) #Decodificamos la URL (esta en ANSII) 
    
    #Parseamos la URL para quedarnos con la parte que nos interesa
    splitDecode = decode.split("=") 
    print(splitDecode)
    
    nastyurl = splitDecode[3]
    print(nastyurl)
    
    parseo = parse('{}","'+ phenotype +'")',nastyurl)
    print(parseo[0])

    

def readNEWURL():
    
    newURL = ws['B3'].value    
    decode = unquote(newURL)
    splitDecode = decode.split("=") 
    nastyurl = splitDecode[3]
    uri = nastyurl.split('&')[0]
    print(uri)        

#readNEWURL()

def readResolution():
    resolution = ws['E3'].value
    print(resolution)

readResolution()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 15 13:20:30 2019

@author: khaosdev
"""


from SPARQLWrapper import SPARQLWrapper, JSON, XML, N3, RDF

sparql_service = "http://sparql.bioontology.org/sparql/"

#To get your API key register at http://bioportal.bioontology.org/accounts/new
api_key = "ca0787e8-0bd8-4538-846f-91ccccb748b4"

#Some sample query.
query_string = """ 
    PREFIX omv: <http://omv.ontoware.org/2005/05/ontology#>
    SELECT ?ont ?name ?acr
    WHERE { ?ont a omv:Ontology;
                 omv:acronym ?acr;
                 omv:name ?name .
    } 
"""
sparql = SPARQLWrapper(sparql_service)
sparql.addCustomParameter("apikey",api_key)
sparql.setQuery(query_string)
sparql.setReturnFormat(JSON)
results = sparql.query().convert()
for result in results["results"]["bindings"]:
    print (result["ont"]["value"], result["name"]["value"], result["acr"]["value"])
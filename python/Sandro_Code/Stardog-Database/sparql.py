#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 20 09:48:53 2018

@author: Sandro
"""

import logging
from string import Template

from SPARQLWrapper import SPARQLWrapper, JSON
from SPARQLWrapper.SPARQLExceptions import QueryBadFormed

LOGGER = logging.getLogger('main')


class SparQLEndpoint:
    """ Defines an SPARQL endpoint.
    """

    QUERIES_PATH = '/app/app/resources/query/'

    def __init__(self, endpoint: str, username: str=None, pwd: str=None):
        self.sparql = SPARQLWrapper(endpoint + '/query')
        self.sparql.setReturnFormat(JSON)

        self.sparql_update = SPARQLWrapper(endpoint + '/update')
        self.sparql_update.setReturnFormat(JSON)

        if username:
            self.sparql.setCredentials(username, pwd)
            self.sparql_update.setCredentials(username, pwd)

    def perform_update_query(self, query: str):
        """ Perform a SPARQL update query on the endpoint.

        :param query: String query.
        """
        self.sparql_update.setQuery(query)
        LOGGER.debug('Executing (update) query: ' + query.replace('\n', ' '))

        return self.sparql_update.query().convert()

    def perform_query(self, query: str):
        """ Perform a SPARQL query on the endpoint.

        :param query: String query.
        """
        self.sparql.setQuery(query)
        LOGGER.debug('Executing query: ' + query.replace('\n', ' '))

        return self.sparql.query().convert()

    def precomputed_query(self, name: str, **kwargs):
        """ Perform a query from a file.

        :param name: Name of the file containing the query.
        :param ppformat: Return only values.
        """
        computed_path = self.QUERIES_PATH + name

        try:
            query = Template(open(computed_path).read())
            query = query.substitute(kwargs)
            response = self.perform_query(query)
        except FileNotFoundError:
            LOGGER.warning('Query {} not found at {}'.format(name, computed_path))
            raise Exception('Precomputed query not found')
        except QueryBadFormed:
            raise Exception('Query bad formed')

        LOGGER.debug('Response to query: {}'.format(response))

        if response:
            if 'results' in response:
                response = response['results']['bindings']

                try:
                    if 'valueProperty' in response[0]:
                        response = [prop['valueProperty']['value'] for prop in response]
                except IndexError:
                    pass

            elif 'boolean' in response:
                response = response['boolean']

        return response

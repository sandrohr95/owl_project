# -*- coding: utf-8 -*-
"""
Created on Tue Jul 24 12:10:25 2018


@author: mmar and Sandro
"""

####################################################
# Para escribir label, isDefinedBy y definition para una clase de la ontología
from owlready2 import AnnotationProperty, get_ontology
import pandas as pd

name_ontology = '/home/khaosdev/AnacondaProjects/TFM/owl_project/python/Sandro_Code/data/GRO2.owl'
csv_name = '/home/khaosdev/AnacondaProjects/TFM/owl_project/python/Sandro_Code/data/owlProperties.csv'
modify_owl = '/home/khaosdev/AnacondaProjects/TFM/owl_project/python/Sandro_Code/data/modify.owl'

def read_owl_properties(csv_name):
    df = pd.read_csv(csv_name)
    df_uri = df['NEW_URI']
    df_labels = df['LABELS']
    df_subclass = df['SUBCLASSOF']
    df_definition = df['DEFINITION']
    df_part_of = df['PART_OF']
    df_isDefinedBy = df['ISDEFINEDBY']

    return df_labels, df_subclass, df_definition, df_uri, df_part_of, df_isDefinedBy


def add_properties(name_ontology, csv_name, modify_owl):
    list_label, list_subclass, list_def, list_new_uri, list_part_of, list_isDefinedBy = read_owl_properties(csv_name)

    onto = get_ontology(name_ontology).load()

    # we create a new annotation for definitions and labels
    with onto:
        class imported_definition(AnnotationProperty):
            pass

        class imported_label(AnnotationProperty):
            pass

        class importedAxiom(AnnotationProperty):
            pass

    for i, uri in enumerate(list_new_uri):
        print('HERE')
        for c in onto.classes():
            cc = c.iri  # Nos devuelve la uri completa
            if cc == uri:  # Si coincide la URI le cambiamos label, definition and subClassOf
                if str(list_subclass[i]) != 'nan':
                    axioms = list_subclass[i].split('\'')
                    for a in axioms:
                        if 'http' in a:
                            c.importedAxiom.append("is a " + str(a))
                if str(list_part_of[i]) != 'nan':
                    p_axioms = list_part_of[i].split('\'')
                    for p in p_axioms:
                        if 'http' in p:
                            c.importedAxiom.append("part_of "+ str(p))

                if str(list_label[i]) != 'nan':
                    c.imported_label = str(list_label[i])
                
                if str(list_isDefinedBy[i]) != 'nan':
                     c.isDefinedBy.append(str(list_isDefinedBy[i]))

                if str(list_def[i]) != 'nan':
                    c.imported_definition = str(list_def[i])

    onto.save(file=modify_owl, format="rdfxml")


#add_properties(name_ontology, csv_name,modify_owl)

import os
import csv
import codecs


######################################################################
# Esta clase nos sirve para leer las URIS ya hemos pasado a un fichero csv previamente (estado la antigua,nueva URI)

def renameTotxt(filename):
    # calcular la longitud de la cadena
    l = len(filename)
    # calculamos la longitud sin la extension
    newl = l - 4
    # Obtenemos el nombre del fichero sin extension
    name = filename[0:newl]
    # obtenemos el nuevo nombre
    newname = name + ".txt"
    # renombramos el fichero
    os.rename(filename, newname)

    return newname


def renameToOWL(newname):
    # calcular la longitud de la cadena
    l = len(newname)
    # calculamos la longitud sin la extension
    newl = l - 4
    # Obtenemos el nombre del fichero sin extension
    name = newname[0:newl]
    # obtenemos el nuevo nombre
    oldname = name + ".owl"
    # renombramos el fichero
    os.rename(newname, oldname)


def changeURIs(filename):
    # Leemos la ontología como formato .txt
    newname = renameTotxt(filename)

    oldUri = '<rdfs:label>' + 'RNA' + '</rdfs:label>'
    newUri = '<rdfs:label>' + 'NUEVA ETIEUQTEA COMPISSS' + '</rdfs:label>'

    #        print(oldUri)
    #        print(newUri)

    # Abre la ontología y la lee
    file = codecs.open(newname, "r", "utf-8")
    buff = file.read()  # Lee la ontología y la muestra por pantalla
    #        print(buff)
    #  Reemplaza todos las cadenas que coincidan en la ontologían
    rbuff = buff.replace(oldUri, newUri)
    #        print('NUEVA ONTOLOGÍA')
    #        print(rbuff)
    # Abre la ontología y escribe en ella
    file = codecs.open(newname, "w", "utf-8")
    # file = open(newname, "w")
    file.write(rbuff)
    file.close()


    # Vuelve a ponerle la extensión .owl
    renameToOWL(newname)

changeURIs("graonew.owl")








#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  3 13:05:07 2018

@author: Sandro Hurtado Requena
"""
import csv
import openpyxl
from urllib.parse import unquote

excel_path = '/home/khaosdev/AnacondaProjects/TFM/owl_project/python/class_equivalent.xlsx'
initial_row = 3
n_row = 365


def read_from_excel(name_Excel):
    wb = openpyxl.load_workbook(name_Excel)
    ws = wb[wb.sheetnames[0]]

    return ws


def read_old_uri(initial_row, n_row, excel_path):
    ws = read_from_excel(excel_path)
    # Recorremos la primera columna del excel(A), y la descodificamos siempre y cuando no sea nulo su valor y sea un hiperlink
    list_uri = []
    list_class = []
    # Otra opción para coger las columnas:   for col in ws['A']: colA = col.value...

    # La variable x indica el número de filas que queremos coger de nuestro excel
    for x in range(initial_row, n_row):
        colA = ws.cell(row=x, column=1).value

        if colA is None:
            colA = '-'
            uri = colA
            classname = colA

        if (colA != None):
            colAdecode = unquote(colA)  # decodificamos
            # print(colAdecode)
            splitDecode = colAdecode.split("=")
            #            print(splitDecode)
            # print(len(splitDecode))
            if (len(splitDecode) > 1):  # Siempre que entre en un hiperlink
                nastyuri = splitDecode[3]  # Cogemos la parte que nos interesa
                uri = nastyuri.split('"')[0]
                classname = uri.split('#')[-1]
            else:
                uri = splitDecode[0]
                classname = splitDecode[0]

            list_uri.append(uri)
            list_class.append(classname)

    return list_uri, list_class


# olduri, classname =readOldURI(filainicial,nfilas,name_Excel)

# ATENCIÓN: los hiperlinks de los candidatos no son homogéneos por lo que tendremos que aplicar una serie de pasos para "limpiarlos"

def parse_Candidates(initial_row, n_row, excel_path, n_column):
    ws = read_from_excel(excel_path)
    list_class_name = []  # Lista de nombres de la ontología de la que proviene la URI EJ: EFO
    list_final_uri = []

    for x in range(initial_row, n_row):
        col = ws.cell(row=x, column=n_column).value

        if col is None:
            col = '-'
            finaluri = col

            Cname = col

        else:
            coldecode = unquote(col)
            splitDecode = coldecode.split('http')
            if (len(splitDecode) > 1):
                nastyuri = splitDecode[-1]  # Me quedo con el último
                urihttp = 'http' + nastyuri
                Cname = urihttp.split('"')[2]
                uri = urihttp.split('&')[0]
                finaluri = uri.split('"')[0]

            else:
                finaluri = splitDecode[0]

        list_final_uri.append(finaluri)
        list_class_name.append(Cname)
        # Ya tenemos la Uri de todas las ontologías, ahora las limpiaremos y le sacaremos propiedades

    listOwlUri, listOwlName, listFailOwlUri, listCURI = clean_uri_method(list_final_uri)
    #    print(listCURI)

    return listCURI, list_class_name, listOwlUri, listOwlName, listFailOwlUri


# parse_Candidates(3,365,name_Excel,3)

# Con este método limpiaremos algunas uris que tiene nombre de clase GO:0005681 --> GO_0005681 por ejemplo
# Algunas uris seran desechadas y guardadas en failOwl_Uris
# Además sacaremos algunas partes de las uris que nos harán falta en siguientes pasos

def clean_uri_method(list_final_uri):
    listCURI = []  # Lista de Uris de una clase de una ontología EJ:http://www.ebi.ac.uk/efo/EFO_0000651
    listFailOwlUri = []  # Nombre de las ontologías que no podemos parsear
    listOwlUri = []  # Lista de Uris de una ontologías (sin clase) EJ: http://www.ebi.ac.uk/efo/
    listOwlName = []  # Nombre de la clase EJ:EFO_0000651

    fail_uri = '-'

    for uri in list_final_uri:
        #        print(uri)

        if '#' in uri:
            clase = uri.split('#')[-1]
            owluri = uri.split(clase)[0]
            clean_uri = uri

            fail_uri = '-'

        else:
            clase = uri.split('/')[-1]
            if clase == "":  # https://research.bioinformatics.udel.edu/pro/entry/PR:Q9ZUG9/
                countlength = len(uri.split('/'))
                # print(countlength-1)
                # print(uri.split('/'))
                clase = uri.split('/')[countlength - 2]
                # print(clase)
                owluri = uri.split(clase)[0]

                fail_uri = owluri + clase

                # print(uri)
            elif ":" in clase:
                owluri = uri.split(clase)[0]
                clase = clase.replace(":", "_")
                clean_uri = owluri + clase
            else:
                clean_uri = uri
                owluri = uri.split(clase)[0]

        listCURI.append(clean_uri)
        listFailOwlUri.append(fail_uri)
        listOwlUri.append(owluri)
        listOwlName.append(clase)

    return listOwlUri, listOwlName, listFailOwlUri, listCURI


def read_resolution(filainicial, nfilas, name_Excel):
    ws = read_from_excel(name_Excel)

    listResolution = []

    for x in range(filainicial, nfilas):
        resolution = ws.cell(row=x, column=5).value
        if resolution is None:
            finalresolution = '-'
        else:
            finalresolution = resolution

        listResolution.append(finalresolution)

    return listResolution


def list_agreements(filainicial, nfilas, name_Excel):
    ws = read_from_excel(name_Excel)

    listAgreements = []
    for x in range(filainicial, nfilas):
        colW = ws.cell(row=x, column=23).value

        if colW is None:
            agreement = '-'
        else:
            agreement = colW

        listAgreements.append(agreement)
    return listAgreements


# Escritura de un Csv limpio con los campos que nos interesan
def create_csv(nameCSV, filainicial, nfilas, name_Excel, ncolC1, ncolC2):
    listUri, listClassname = read_old_uri(filainicial, nfilas, name_Excel)

    listcandidate1_URI, listcandidate1_name, listOwl1Uri, listOwl1Name, listFail1OwlUri = parse_Candidates(filainicial,
                                                                                                           nfilas,
                                                                                                           name_Excel,
                                                                                                           ncolC1)
    listcandidate2_URI, listcandidate2_name, listOwl2Uri, listOwl2Name, listFailOwl2Uri = parse_Candidates(filainicial,
                                                                                                           nfilas,
                                                                                                           name_Excel,
                                                                                                           ncolC2)

    listResolution = read_resolution(filainicial, nfilas, name_Excel)
    Agreements = list_agreements(filainicial, nfilas, name_Excel)

    with open(nameCSV, 'w') as csvfile:
        fieldnames = ['Classname', 'OLD_URI', 'OWL_Candidate1_Name', 'OWL_Candidate1_URI', 'OWL_Candidate2_Name',
                      'OWL_Candidate2_URI', 'Resolution', 'Agreements', 'Fails_OWL_URI1', 'Fails_OWL_URI2',
                      '(query)OWL_C1URI',
                      'OWL_C1NAME', '(query)OWL_C2URI', 'OWL_C2NAME']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for index, oldURI in enumerate(listUri):
            writer.writerow({'Classname': listClassname[index], 'OLD_URI': oldURI,
                             'OWL_Candidate1_Name': listcandidate1_name[index],
                             'OWL_Candidate1_URI': listcandidate1_URI[index],
                             'OWL_Candidate2_Name': listcandidate2_name[index],
                             'OWL_Candidate2_URI': listcandidate2_URI[index], 'Resolution': listResolution[index],
                             'Agreements': Agreements[index],
                             'Fails_OWL_URI1': listFail1OwlUri[index], 'Fails_OWL_URI2': listFailOwl2Uri[index],
                             '(query)OWL_C1URI': listOwl1Uri[index]
                                , 'OWL_C1NAME': listOwl1Name[index], '(query)OWL_C2URI': listOwl2Uri[index],
                             'OWL_C2NAME': listOwl2Name[index]})

# createCSV('prueba_clean.csv',3,365,name_Excel,2,3)

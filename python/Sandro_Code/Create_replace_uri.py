#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 11 14:33:37 2018

@author: Sandro Hurtado Requena
"""
import pandas as pd
import csv

""" Para realizar este método, los primero que tenemos que hacer es mirar la columna(Agreements), si esta es igual= yes, entonces
pasaremos a mirar la columna(resolution) que coincidirá con alguna de las columnas de candidatos.
"""


def decision_method_agreement(nameCSV, replace_csv):
    df = pd.read_csv(nameCSV)
    classname = df['Classname']
    old_Uri = df['OLD_URI']
    agreements = df['Agreements']
    resolution = df['Resolution']
    candidate1 = df['OWL_Candidate1_Name']
    candidate2 = df['OWL_Candidate2_Name']
    c1_Uri = df['OWL_Candidate1_URI']
    c2_Uri = df['OWL_Candidate2_URI']

    c1_Uri_Owl = df['(query)OWL_C1URI']
    c1_Name_Owl = df['OWL_C1NAME']
    c2_Uri_Owl = df['(query)OWL_C2URI']
    c2_Name_Owl = df['OWL_C2NAME']

    listOldUri = []
    listClassname = []
    listNewUri = []
    listNewName = []
    listNewUriOwl = []
    listNewNameOwl = []

    for i, a in enumerate(agreements):
        if a == 'Yes':  # Siempre que esten de acuerdo miramos la resolución

            if candidate1[i] in resolution[
                i]:  # Si el nombre de la ontología del candidato está en la resolución metemos el candidato
                if 'http' in c1_Uri[i]:  # Siempre que sea una Uri la metemos (hay algunas que son palabras sueltas)
                    if 'http://amigo.geneontology.org/amigo/term/' in c1_Uri[i]:  # La sustituyo por la uri de bioportal
                        classnamec1 = c1_Uri[i].split('/')[-1]
                        bioportal_uri1 = "http://purl.obolibrary.org/obo/" + classnamec1
                        listNewUri.append(bioportal_uri1)
                        listNewUriOwl.append("http://purl.obolibrary.org/obo/")
                    else:
                        listNewUri.append(c1_Uri[i])
                        listNewUriOwl.append(c1_Uri_Owl[i])

                    #                    listNewUri.append(c1_Uri[i])
                    listNewName.append(candidate1[i])
                    listOldUri.append(old_Uri[i])
                    listClassname.append(classname[i])
                    #                    listNewUriOwl.append(c1_Uri_Owl[i])
                    listNewNameOwl.append(c1_Name_Owl[i])

            elif candidate2[i] in resolution[i]:
                if 'http' in c2_Uri[i]:
                    if 'http://amigo.geneontology.org/amigo/term/' in c2_Uri[i]:  # La sustituyo por la uri de bioportal
                        classnamec2 = c2_Uri[i].split('/')[-1]
                        bioportal_uri2 = "http://purl.obolibrary.org/obo/" + classnamec2
                        listNewUri.append(bioportal_uri2)
                        listNewUriOwl.append("http://purl.obolibrary.org/obo/")

                    else:
                        listNewUri.append(c2_Uri[i])
                        listNewUriOwl.append(c2_Uri_Owl[i])

                    listNewName.append(candidate2[i])
                    #                     listNewUri.append(c2_Uri[i])
                    listOldUri.append(old_Uri[i])
                    listClassname.append(classname[i])
                    #                     listNewUriOwl.append(c2_Uri_Owl[i])
                    listNewNameOwl.append(c2_Name_Owl[i])

    with open(replace_csv, 'w') as csvfile:
        fieldnames = ['Classname', 'OLD_URI', 'NEW_URI', 'Name_Ontology', 'URI_Ontology', 'Class_Ontology']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for index, owlname in enumerate(listClassname):
            writer.writerow({'Classname': owlname, 'OLD_URI': listOldUri[index], 'NEW_URI': listNewUri[index],
                             'Name_Ontology': listNewName[index], 'URI_Ontology': listNewUriOwl[index],
                             'Class_Ontology': listNewNameOwl[index]})

# create_ReplaceUri_Csv(nameCSV,name_ReplaceCSV)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 12 11:57:36 2018

@author: Sandro Hurtado Requena

"""
import ReadFromExcel as read_excel
import Create_replace_uri as replace_csv
import replace_uris as replace_owl
import sparql_bioportal_Sandro as sparql
import modify_owl as owlapi

#IMPORTANTE: el fichero excel tiene que tenre en la primera fila la cabecera para poder leerlo
excel_path = '/home/khaosdev/Documentos/Sandro/Master_ISIA/TrabajoFinMaster/python/class_equivalent.xlsx'
n_row = 365 #Número de filas que queremos leer de nuestro excel
initial_row = 3 #Número de la fila por la que empezamos a leer el excel
clean_csv = 'data/Clean.csv' #Nombre del nuevo CSV que creamos para limpiar el excel
replace_uri_csv = 'data/Replace_uri.csv' #Nombre del fichero definitivo CSV que reemplaza las OLD_URI por NEW_URI
""" Columnas de los candidatos en el excel
"""
candidate1_column = 2
candidate2_column = 3
owl_properties_csv = 'data/owlProperties.csv'

ontology_name = 'data/GRO2.owl' #Nombre de la ontología en la que queremos sustituir las URIS
modify_owl = 'data/Owl_mofify.owl'

if __name__ == '__main__':
    
    """ Procesamos el excel y lo insertamos en un CSV
    """
    read_excel.create_csv(clean_csv, initial_row, n_row, excel_path, candidate1_column, candidate2_column)

    """ Creamos un CSV a partir del anterior con OLD y NEW URIS y algunas otras columnas que nos facilitarán las consultas en Sparql
    """
    replace_csv.decision_method_agreement(clean_csv,replace_uri_csv)

    """ Reemplazaremos las OLD_URIS por NEW_URIS en nuestra ontología
    """
    replace_owl.changeURIs(ontology_name,replace_uri_csv)

    """ Obtendremos las 'properties' en Bioportal de cada una de las nuevas URIS que le hemos insertado a la ontología
    """
    sparql_endpoint = sparql.SparqlEndpoint()
    
    list_label,list_def,list_new_uri,list_subclassOf = sparql_endpoint.get_properties_from_bioportal(replace_uri_csv,owl_properties_csv)

    """ Por último le insertamos las properties a las nuevas URIS de la ontología
    """
    owlapi.add_properties(ontology_name,owl_properties_csv, modify_owl)

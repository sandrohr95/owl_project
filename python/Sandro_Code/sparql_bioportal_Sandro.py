# -*- coding: utf-8 -*-
"""
Created on Mon Jul 23 15:06:57 2018

Querying BioPortal SPARQL EndPoint

@author: mmar and Sandro
"""
#############################################
# Obtenemos la definición y label de las Uris que hemos utilizado para reemplazar

from SPARQLWrapper import SPARQLWrapper, JSON
import pandas as pd
import csv

replace_csv = '/home/khaosdev/AnacondaProjects/TFM/owl_project/python/Sandro_Code/data/Replace_uri.csv'
owl_properties_csv = '/home/khaosdev/AnacondaProjects/TFM/owl_project/python/Sandro_Code/data/owlProperties.csv'



""" Método para realizar consultas en Bioportal
"""
class SparqlEndpoint:
    def __init__(self):
       sparql_service = "http://sparql.bioontology.org/sparql/"
        # To get your API key register at http://bioportal.bioontology.org/accounts/new
       api_key = "ca0787e8-0bd8-4538-846f-91ccccb748b4"
       self.sparql = SPARQLWrapper(sparql_service)
       self.sparql.addCustomParameter("apikey", api_key)

            
    def get_properties_from_bioportal(self, replace_csv, owl_properties_csv):
        
        df = pd.read_csv(replace_csv)
        new_uri = df['NEW_URI']
        owl_class = df['Name_Ontology']
        owl_uri = df['URI_Ontology']
        owl_name = df['Class_Ontology']
        
        list_label = []
        list_isDefineBy = []
        list_def = []
        list_subclassOf = []
        list_new_uri = []
        list_part_of = []
    
        for i, uri in enumerate(owl_uri):
    
            label_query = """
             PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
             PREFIX pref: <http://bioportal.bioontology.org/metadata/def/>
             PREFIX go: <%s>
             SELECT  ?label  ?prefLabel
             FROM <http://bioportal.bioontology.org/ontologies/%s>
             WHERE
                {
                    OPTIONAL{ go:%s rdfs:label ?label.}
                    OPTIONAL{ go:%s pref:prefLabel  ?prefLabel.}
    
                }
             """ % (uri, owl_class[i], owl_name[i], owl_name[i])
             
            isDefinedBy_query = """
                PREFIX omv: <http://omv.ontoware.org/2005/05/ontology#>
                SELECT ?ont ?name ?acr       
                WHERE {
                	?ont a omv:Ontology .
                	?ont omv:acronym ?acr .
                	?ont omv:name ?name .
                    FILTER( CONTAINS(?acr ,'%s'))
                  }
             """ % (owl_class[i])
    
            def_query = """
             PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
             PREFIX go: <%s>
             SELECT  ?def ?definition  
             FROM <http://bioportal.bioontology.org/ontologies/%s>
             WHERE
                {
                     OPTIONAL{ go:%s go:def ?def.}
                     OPTIONAL{ go:%s go:definition ?definition.}
                }
             """ % (uri, owl_class[i], owl_name[i], owl_name[i])
    
            subClassOf_query = """
             PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
             PREFIX go: <%s>
             SELECT  ?subClassOf  
             FROM <http://bioportal.bioontology.org/ontologies/%s>
             WHERE
                {
                     OPTIONAL{ go:%s rdfs:subClassOf ?subClassOf.}
    
                }
             """ % (uri, owl_class[i], owl_name[i])
    
            part_of_query = """
                PREFIX go: <%s>
                PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                PREFIX meta: <http://bioportal.bioontology.org/metadata/def/>      
                PREFIX owl:  <http://www.w3.org/2002/07/owl#>       
                PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                
                SELECT DISTINCT  ?obj2 ?part_of
                WHERE
                {
                  go:%s rdfs:subClassOf ?obj1.
                  ?obj1 <http://www.w3.org/2002/07/owl#onProperty> ?obj2.
                  ?obj1 <http://www.w3.org/2002/07/owl#someValuesFrom> ?part_of. 
                  
                  FILTER regex(str(?obj2),"http://purl.obolibrary.org/obo/BFO_0000050",'i')
                }
             """ % (uri, owl_name[i])
    
            self.sparql.setQuery(label_query)
            self.sparql.setReturnFormat(JSON)
            results = self.sparql.query().convert()
            label = results["results"]["bindings"]
            
            self.sparql.setQuery(isDefinedBy_query)
            self.sparql.setReturnFormat(JSON)
            results = self.sparql.query().convert()
            isdefinedBy = results["results"]["bindings"]
            
            self.sparql.setQuery(def_query)
            self.sparql.setReturnFormat(JSON)
            results = self.sparql.query().convert()
            definition = results["results"]["bindings"]
            
            self.sparql.setQuery(subClassOf_query)
            self.sparql.setReturnFormat(JSON)
            results = self.sparql.query().convert()
            subClassOf = results["results"]["bindings"]
            
            self.sparql.setQuery(part_of_query)
            self.sparql.setReturnFormat(JSON)
            results = self.sparql.query().convert()
            part_of = results["results"]["bindings"]
        
    
            list_new_uri.append(new_uri[i])
            if len(label) != 0:
                if label[0]["label"]:
                    list_label.append(label[0]["label"]['value'])
                else:
                    list_label.append(label[0]["prefLabel"]['value'])
            else:
                list_label.append("")
                
            if len(isdefinedBy) != 0:
                if isdefinedBy[0]["name"]:
                    if isdefinedBy[0]["acr"]['value'] == owl_class[i]:
                        list_isDefineBy.append(isdefinedBy[0]["name"]['value']+' '+
                                               isdefinedBy[0]["acr"]['value'])
                    else:
                        list_isDefineBy.append(isdefinedBy[1]["name"]['value']+' '+
                                               isdefinedBy[1]["acr"]['value'])
            else:
                list_isDefineBy.append("")
    
            if len(definition) != 0:
                if definition[0]["def"]:
                    list_def.append(definition[0]["def"]['value'])
                else:
                    list_def.append(definition[0]["definition"]['value'])
            else:
                list_def.append("")
    
            if len(subClassOf) != 0:
                sublist_subclassOf = []
                for s in subClassOf:
                    sublist_subclassOf.append(s['subClassOf']['value'])
                list_subclassOf.append(sublist_subclassOf)
            else:
                list_subclassOf.append("")
    
            if len(part_of) != 0:
                sublist_part_of = []
                for p in part_of:
                    list_part_of.append(p['part_of']['value'])
                list_part_of.append(sublist_part_of)
            else:
                list_part_of.append("")
    
        with open(owl_properties_csv, 'w') as csvfile:
            fieldnames = ['NEW_URI', 'LABELS', 'SUBCLASSOF', 'DEFINITION', 'PART_OF', 'ISDEFINEDBY']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            for index, newuri in enumerate(list_new_uri):
                writer.writerow({'NEW_URI': newuri, 'LABELS': list_label[index],
                                 'SUBCLASSOF': list_subclassOf[index], 'DEFINITION': list_def[index],
                                 'PART_OF': list_part_of[index], 'ISDEFINEDBY': list_isDefineBy[index]})
    
        return list_new_uri, list_label, list_def, list_subclassOf

#sparql = SparqlEndpoint()
#sparql.get_properties_from_bioportal(replace_csv, owl_properties_csv)
